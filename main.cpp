#include <iostream>
#include "sort.hpp"

template<typename T>
void print_vector(std::vector<T> &vect) {
  std::cout << '[';

  size_t i;
  for (i = 0; i<vect.size()-1; i++) {
    std::cout << vect[i] << ", ";
  }

  std::cout << vect[i] << ']' <<std::endl;
}

int main() {
  std::string value;
  std::vector<std::string> values;

  std::cout <<"Enter some values (\"/\" to stop) :" << std::endl;

  while (value != "/") {
    std::getline(std::cin, value);
    values.push_back(value);
  }

  values.pop_back(); // remove the "/"

  std::cout << "Entered values: ";
  print_vector(values);

  selectionsort(values);

  std::cout << "Sorted values: ";
  print_vector(values);
}
